import React from 'react';
import { mount } from 'enzyme';
import Root from 'Root';
import CommentList from 'components/CommentList';

const initialState = {
    comments: ['Comment 1', 'Comment 2']
};

let wrapper;

beforeEach(() => {
    wrapper = mount(
        <Root initialState={initialState}>
            <CommentList />
        </Root>
    );
});

afterEach(() => {
    wrapper.unmount();
});

it('creates one li per comment', () => {
    expect(wrapper.find('li').length).toEqual(2);
});

it('shows every of the comments', () => {
    const renderedText = wrapper.render().text();

    expect(renderedText).toContain('Comment 1');
    expect(renderedText).toContain('Comment 2');
});
