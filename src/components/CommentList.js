import React from 'react';
import { connect } from 'react-redux';

const CommentList = ({ comments }) => {
    const renderComments = () => {
        return comments.map((comment, index) => {
            return (
                <li key={index}>
                    {comment}
                </li>
            );
        });
    }

    return (
        <div>
            <ul className="ui list">
                {renderComments()}
            </ul>
        </div>
    );
}

const mapStateToProps = ({ comments }) => ({ comments });

export default connect(mapStateToProps)(CommentList);