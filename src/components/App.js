import React from 'react';
import CommentBox from 'components/CommentBox';
import CommentList from 'components/CommentList';

const App = () => {
  return (
    <div className="ui container" style={{ marginTop: '20px' }}>
      <CommentBox />
      <CommentList />
    </div>
  );
}

export default App;
