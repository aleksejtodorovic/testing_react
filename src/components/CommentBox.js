import React, { useState } from "react";
import { connect } from "react-redux";
import { saveComment, fetchComments } from "actions";

const CommentBox = ({ saveComment, fetchComments }) => {
  const [comment, setComment] = useState('');

  const onSubmit = evt => {
    evt.preventDefault();
    saveComment(comment);
    setComment('');
  };

  return (
    <div className="ui segment" onSubmit={onSubmit}>
      <form className="ui form">
        <div className="field">
          <h4>Add a comment</h4>
          <textarea
            value={comment}
            onChange={({ target: { value } }) => setComment(value)}
          />
        </div>
        <button className="ui button" type="submit">
          Submit
          </button>
      </form>
      <button
        id="fetch-comments"
        className="ui button"
        style={{ marginTop: "10px" }}
        onClick={fetchComments}
      >
        Fetch Comments
        </button>
    </div>
  );
}

export default connect(
  null,
  { saveComment, fetchComments }
)(CommentBox);
